
problem <- function(H,D)
{

  evaluate <- function(solution)
  {
    eval <- 0
    p = as.numeric(solution)
    for(i in 1:length(p))
    {
      for(j in 1:length(p))
      {
        eval <- eval + H[i,j] * D[p[i],p[j]]
      }
    }
    return(eval)
  }
  return(evaluate)
}

H <- matrix(runif(100),nrow = 10) 
D <- matrix(runif(100),nrow = 10) 

ev <- problem(H,D)
sol <- permutation(1:10)
#ev(sol)

isol <- randomPermutation(10)
res <- basicLocalSearch(ev, isol, neighborhood = exchangeNeighborhood(isol), selector =  greedySelector,  resources = cResource((time=10)))
plotProgress(res)

res <- basicAco(evaluate = ev, nants = 100, pheromones = permuLinkPheromone(initial.trail = matrix(rep(1,100),10),0.1),resources = cResource(time=10)